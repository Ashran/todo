// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    
    apiKey: "AIzaSyCuci1j-jXp_Q435xJKYEj9Rkl962GVc8g",
    authDomain: "todolistapp-ace0e.firebaseapp.com",
    databaseURL: "https://todolistapp-ace0e.firebaseio.com",
    projectId: "todolistapp-ace0e",
    storageBucket: "todolistapp-ace0e.appspot.com",
    messagingSenderId: "668515791392"
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
